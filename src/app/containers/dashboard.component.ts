import {Component, OnInit} from '@angular/core';
import {ApiService} from '../services/api-service';
import {User} from '../models/user';

@Component({
  selector: 'app-dashboard',
  template: `
    <div fxLayout="row wrap" fxLayoutAlign="start center">
      <mat-card *ngFor="let user of users">
        <img mat-card-avatar [src]="user.avatar"/>
        <mat-card-title>{{user.first_name}}{{user.last_name}}</mat-card-title>
        <mat-card-content>{{user.email}}</mat-card-content>
      </mat-card>
    </div>
  `,
  styles: [`
    mat-card {
      padding: 2rem;
      margin: 3rem;
    }
  `]
})

export class DashboardComponent implements OnInit {
  users: User[] = [];
  loading = true;

  constructor(private apiService: ApiService) {
  }

  ngOnInit() {
    this.apiService.getUserList().subscribe(users => {
      this.users = users;
    });
  }
}
