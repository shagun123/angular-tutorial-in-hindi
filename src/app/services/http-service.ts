import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';


console.log('hello i am http service');

@Injectable()
export class HttpService {
  private baseUrl = 'https://reqres.in/api';

  constructor(private httpClient: HttpClient) {
  }

  get(url: string): Observable<any> {
    return this.httpClient.get(this.baseUrl + url)
      .pipe(catchError(this.handleErrors.bind(this)));
  }

  post(url: string, data: any): Observable<any> {
    return this.httpClient.post(this.baseUrl + url, data).pipe(catchError(this.handleErrors.bind(this)));
  }

  private handleErrors(response) {
    alert(response.message);
    return throwError({message: response.message});
  }
}
